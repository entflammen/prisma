import copy
import re
import discord
from enum import Enum
import itertools
import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from discord.ext import commands
from prisma import Context, UserError, style, menus, db
from prisma.bot import Config
from prisma.cogs import Cog, make_cog_setup

class RoleType(Enum):
    '''Enum to set roletype names'''
    SEXUALITY = "sexuality"
    GENDER = "gender"
    COUNTRY = "country"
    AGE = "age"
    PRONOUN = "pronoun"
    ROMANTIC = "romantic"

class Role(db.Model):
    '''Model class to represent the Role table'''
    __tablename__ = "roles"
    rankname = sa.Column('rankname', sa.Text, nullable=False)
    rankid = sa.Column('rankid', sa.BigInteger, nullable=False, primary_key=True)
    joinable = sa.Column('joinable', sa.Boolean, nullable=False)
    pingable = sa.Column('pingable', sa.Boolean, nullable=False)
    ranktype = sa.Column('type', pg.ENUM(RoleType, name="role_type"), nullable=True)
    #children = sa.relationship("Alias", cascade="all,delete", backref="roles")
    
    
class Alias(db.Model):
    '''Model Class to represent the Alias table'''
    __tablename__= "alias"
    id = sa.Column(sa.Integer, primary_key=True)
    rankid = sa.Column('rankid', sa.BigInteger, sa.ForeignKey('roles.rankid'), nullable=False)
    rankname = sa.Column('rankname', sa.Text, nullable=False)
    
class RoleCog(Cog):
    # Role Manager Command Group

    #########################
    # Role Manager Commands #
    #########################
    def __init__(self, client):
        self.client = client

    @commands.dm_only()
    @commands.command()
    async def info(self, ctx: Context):
        '''Sets user roles via Q/A DM conversation.
         Usage: p!info sent via DM to Prisma'''

        ##### Internal Functions #####
        def authorcheck(message):
            return message.author == ctx.author

        async def checkroles(member, roletype):
            categorynames = [role.rankname for role in roles[roletype]]
            for role in member.roles:
                if role.name in categorynames:
                    embed = style.info_embed(
                            "Role found!",
                            f"You already have role **{role.name}** of this type.  Remove it? (React to decide)",
                            title_mode="description")
                    confirmation = await menus.Confirm(embed).prompt(ctx)
                    if confirmation == menus.Confirmation.CONFIRM:
                        await member.remove_roles(role)
                        await ctx.send(f"Role **{role.name}** removed!")
                    if confirmation == menus.Confirmation.CANCEL:
                        await ctx.send(f"Keeping **{role.name}**")

        #FIXME
        lgbtqServer = 331517548636143626
        alertchannelid = 351335296530513920 # Set to staff-team channel
        #alertchannelid = 849043425100693514 # Set to breaking-prisma-repeatedly channel
        timeout = 120 
        author = ctx.message.author
        server = self.client.get_guild(id=lgbtqServer)
        member = server.get_member(author.id)

        sexualities = ctx.db.query(Role).filter(Role.ranktype=="SEXUALITY").all()
        genders = ctx.db.query(Role).filter(Role.ranktype=="GENDER").all()
        countries = ctx.db.query(Role).filter(Role.ranktype=="COUNTRY").all()
        ages = ctx.db.query(Role).filter(Role.ranktype=="AGE").all()
        pronouns = ctx.db.query(Role).filter(Role.ranktype=="PRONOUN").all()
        romantics = ctx.db.query(Role).filter(Role.ranktype=="ROMANTIC").all()

        roles = { "pronoun" : pronouns,
                  "gender" : genders,
                  "sexuality" : sexualities,
                  "romantic" : romantics,
                  "country" : countries,
                  "ages" : ages}
        
        messages = { "welcome" : "Hi, I'm Prisma and I'm going to ask you a few questions to set your personal roles!\n"+
                    "To input multiple answers, please separate responses with `/` or `,`\nTo see a list of options, respond with `options`",
                     "age" : "First off, how old are you?",
                     "pronoun" : "What are your pronouns?",
                     "trans" : "Do you identify as Transgender?",
                     "gender" : "What gender/genders do you identify as?",
                     "sexuality" : "What sexuality/sexualities do you identify as?",
                     "romantic" : "What is your romantic orientation/orientations?",
                     "country" : "Lastly, what country are you from?",
                     "goodbye" : "Your roles are all set.  Enjoy the server!"}

        errors = []
        for item in messages:
            roleflg = False
            while not roleflg:

                # This loop handles all Info roles that are not basic messages, Country, Age, or Trans ID
                await ctx.send(messages[item])
                if item in ["welcome", "goodbye"]:
                    roleflg = True
                if item not in ["welcome", "age", "trans", "country", "goodbye"]:
                    response = await ctx.bot.wait_for('message', check=authorcheck, timeout=timeout)
                    if response.content.lower().strip() == "options":
                        msg = "\n".join([role.rankname for role in roles[item]])
                        await ctx.send(f"**{item} role options: \n**")
                        await ctx.send(f"{msg}")
                    elif response.content.lower().strip() == "skip":
                        roleflg = True
                    else:
                        rolerequest = re.split('/|,',response.content.lower().strip())
                        await checkroles(member, item)
                        for request in rolerequest:
                            for role in roles[item]:
                                # This handling is predicated on the idea that all roles add-able by !info are in one of the following forms:
                                # <Category>: <Role> or <Role>
                                rolesplit = role.rankname.lower().split(': ')
                                if len(rolesplit) > 1:
                                    rolematch = rolesplit[1].strip()
                                else:
                                    rolematch = rolesplit[0].strip()
                                if request == rolematch:
                                    try:
                                        serverrole = server.get_role(role.rankid)
                                        await member.add_roles(serverrole)
                                        roleflg = True
                                    except: 
                                        print(f"PRISMA ROLE ADD FAILURE!  Roletype: {item}")
                                        errors.append(item)
                    if not roleflg and response.content.lower().strip() != "options":
                        await ctx.send(f"Cannot find role {response.content}.\n For a list of role options, respond with `options`.")

                # Set User Country
                if item == "country":
                    response = await ctx.bot.wait_for('message', check=authorcheck, timeout=timeout)
                    if response.content.lower().strip() == "options":
                        msg = "\n".join([role.rankname for role in roles[item]])
                        await ctx.send(f"**{item} role options: \n**")
                        await ctx.send(f"{msg}")
                    elif response.content.lower().strip() == "skip":
                        roleflg = True
                    else:
                        await checkroles(member, item)
                        aliases = ctx.db.query(Alias).all()
                        searchspace = roles[item]+aliases
                        for role in searchspace:
                            rolesplit = role.rankname.lower().split(': ')
                            if len(rolesplit) > 1:
                                rolematch = rolesplit[1].strip()
                            else:
                                rolematch = rolesplit[0].strip()
                            if response.content.lower().strip() == rolematch:
                                try:
                                    serverrole = server.get_role(role.rankid)
                                    await member.add_roles(serverrole)
                                    roleflg = True
                                except:
                                    print("PRISMA ROLE ADD FAILURE! Roletype: Country")
                                    errors.append(item)    
                    if not roleflg and response.content.lower().strip() != "options":
                        # As send menu asking user to confirm.  If Yes, create new role and message #staff-team.  If no, send message about options and wait for
                        # new response
                        #try:
                        embed = style.info_embed(
                            "Confirm Country Role",
                            f"**Country**:{response.content}",
                            title_mode="description")
                        confirmation = await menus.Confirm(embed).prompt(ctx)
                        if confirmation == menus.Confirmation.CONFIRM:

                            # Create Role and add to user
                            newcountry = "Country: "+response.content
                            await server.create_role(name=newcountry)
                            serverroles = server.roles
                            for item in [role for role in serverroles if "Country: " in role.name]:
                                if item.name == newcountry:
                                    entry = Role(rankname=item.name,
                                                 rankid=item.id,
                                                 joinable=False,
                                                 pingable=False,
                                                 ranktype="COUNTRY")
                                    try:
                                        ctx.db.add(entry)
                                        ctx.db.flush()
                                    except Exception:
                                        raise UserError("Role Entry Failed!",
                                                        f"Prisma failed while attempting to enter role {role.name} into the database.")
                                    try: 
                                        serverrole = server.get_role(item.id)
                                        await member.add_roles(serverrole)
                                        roleflg = True
                                    except:
                                        print("PRISMA ROLE ADD FAILURE! Roletype: Country (new)")
                                        errors.append(item)
                                    # Post in Staff-Team for confirmation
                                    embed = style.info_embed(
                                        "Confirm New Country Role Creation",
                                        f"**{newcountry}**",
                                        title_mode="description")
                                    staffchannel = server.get_channel(alertchannelid)
                                    staffctx = copy.copy(ctx)
                                    staffctx.channel = staffchannel
                                    confirmation = await menus.Confirm(embed, timeout=300.0).prompt(staffctx)
                                    if confirmation == menus.Confirmation.CONFIRM:
                                        await staffctx.send(f"Thank you for confirming the creation of the {newcountry} role!")
                                    if confirmation == menus.Confirmation.CANCEL:
                                        await serverrole.delete()
                                        await staffctx.send(f"Role **{serverrole.name}** deleted!")
                            # If staff confirm, do nothing. If staff don't confirm, delete role

                        elif confirmation == menus.Confirmation.CANCEL:
                            await ctx.send("To see a list of country options, respond with `options`")
                        #except Exception:
                        #    raise UserError(f"Prisma Error",f"Problem prompting user for role!")

                            
                # Set User Age Role        
                if item == "age":
                    currentage = [role for role in member.roles if role.name in ["Under 16", "Over 16", "Over 18"]]
                    response = await ctx.bot.wait_for('message', check=authorcheck, timeout=timeout)
                    if response.content.lower().strip() == "skip":
                        roleflg = True
                    else:
                        try:
                            if len(currentage)>0:
                                await member.remove_roles(currentage[0])
                            userage = int(response.content)
                            staffchannel = server.get_channel(alertchannelid)
                            if userage < 13 and userage > 0:
                                # Ask user to confirm they are a wee babby
                                embed = style.info_embed(
                                "Confirm Age",
                                f"You entered age **{userage}**, is this correct?",
                                title_mode="description")
                                confirmation = await menus.Confirm(embed).prompt(ctx)
                                if confirmation == menus.Confirmation.CONFIRM:
                                    # Baby detected, Alert Staff!
                                    await staffchannel.send(f"User **<@{member.id}>** has reported they are age **{userage}**.  Possible TOS violating baby detected!")
                                    roleflg = True
                                if confirmation == menus.Confirmation.CANCEL:
                                    continue
                            if userage < 16 and userage >= 13:
                                # U16 Role
                                agerole = next(role for role in roles["ages"] if role.rankname == "Under 16")
                                serverrole = server.get_role(agerole.rankid)
                                await member.add_roles(serverrole)
                                roleflg = True

                            if userage < 18 and userage >= 16:
                                # O16 Role
                                try:
                                    agerole = next(role for role in roles["ages"] if role.rankname == "Over 16")
                                    serverrole = server.get_role(agerole.rankid)
                                    await member.add_roles(serverrole)
                                    if len(currentage)>0 and currentage[0].name != agerole.rankname:
                                        if currentage[0].name == "Under 16":
                                            await staffchannel.send(f"User **<@{member.id}>** has changed their age role from **{currentage[0].name}** to **{agerole.rankname}**")
                                    roleflg = True
                                except:
                                    print("PRISMA ROLE ADD FAILURE! Roletype: Age")
                                    errors.append(item)

                            if userage >=18:
                                # O18 Role
                                try:
                                    agerole = next(role for role in roles["ages"] if role.rankname == "Over 18")
                                    serverrole = server.get_role(agerole.rankid)
                                    await member.add_roles(serverrole)
                                    if len(currentage)>0 and currentage[0].name != agerole.rankname:
                                        if currentage[0].name in ["Under 16", "Over 16"]:
                                            await staffchannel.send(f"User **<@{member.id}>** has changed their age role from **{currentage[0].name}** to **{agerole.rankname}**")
                                    roleflg = True
                                except:
                                    print("PRISMA ROLE ADD FAILURE! Roletype: Age")
                                    errors.append(item)
                        except ValueError:
                            await ctx.send("Please input a valid number for age")
                        
                if item == "trans":
                    response = await ctx.bot.wait_for('message', check=authorcheck, timeout=timeout)
                    if response.content.lower().strip() == "yes":
                        try:
                            serverrole = server.get_role(477499334640926720)
                            await member.add_roles(serverrole)
                            roleflg = True
                        except:
                            print("PRISMA ROLE ADD FAILURE! Roletype: Trans")
                            errors.append(item)
                    elif response.content.lower().strip() == "no":
                        roleflg = True
                    elif response.content.lower().strip() == "skip":
                        roleflg = True
                    else:
                        await ctx.send("Please respond Yes or No")

        if len(errors)>0:
            errormsg = "Possible error adding roles in the following catagories:\n" 
            for item in errors:
                errormsg += item+"\n"
            errormsg += "Please check your profile roles and report any issues in #staff-hotline"
            embed = style.info_embed("Possible Role Errors",
                                     errormsg,
                                     title_mode="description")
            await ctx.send(embed=embed)
            
    # rolemgr main command #
    @commands.has_any_role('Owner','Admin','Moderator', 'Assistant', 'prisma-dev')
    @commands.group(pass_context=True)
    async def rolemgr(self, ctx: Context):
        if ctx.invoked_subcommand is None:
                embed = style.info_embed(
                        "Role Manager Commands",
                        title_mode="description")
                embed.add_field(name="`new <rolename> <joinable> <pingable>`",value="Add new role to the bot database.  Role must already exist on the server.", inline=False)
                embed.add_field(name="`remove <rolename>`",value="Removes role from bot database.", inline=False)
                embed.add_field(name="`lookup <rolename>`",value="Retrieve information on role <rolename> from the bot database.", inline=False)
                embed.add_field(name="`joinable <rolename>`",value="Toggle a role's `joinable` attribute.  `True` means Users can use the bot to join the role.", inline=False)
                embed.add_field(name="`pingable <rolename>`",value="Toggle a role's `pingable` attribute.  `True` means Users can use the bot to mention the role.", inline=False)
                embed.add_field(name="`sync`",value="Sync database with the server.  Joinable and Pingable variables must be set manually.", inline=False)
                embed.add_field(name="`audit`", value="Returns a table comparing the availability of roles on the server and the bot.", inline=False)
                embed.add_field(name="`revert`",value="Revert the database to initial configuration state.",inline=False)
                await ctx.send(embed=embed)

                
    # Add new role table entry # 
    @rolemgr.command(pass_context=True)
    async def new(self, ctx: Context, name: str, joinable: bool = False, pingable: bool = False):
        role = next(filter(lambda rank: rank.name==name, ctx.guild.roles))
        try:
            embed = style.info_embed(
                "Confirm New Role",
                f"**Role**:{role.name}\n**Opt-in**:{joinable}\n**Pingable**:{pingable}",
                title_mode="description")
            confirmation = await menus.Confirm(embed).prompt(ctx)
            if confirmation == menus.Confirmation.CONFIRM:
                await self.addentry(ctx, role, joinable, pingable)
                await ctx.success(f"Role Added",f"**{role.name}**")
            elif confirmation == menus.Confirmation.CANCEL:
                await ctx.error("Canceled", "Role add request canceled")    
        except Exception:
            raise UserError(f"Prisma Error",f"Could not find role {role.name} on server")

        
    # Remove an entry from the role table #
    @rolemgr.command(pass_context=True)
    async def remove(self, ctx: Context, name: str):
        try:
            role = await self.queryrole(ctx, name)
            roles = "\n".join([item.rankname for item in role])
            embed = style.info_embed(
                "Confirm Role Deletion",
                f"**Role**: {roles}",
                title_mode="description")
            confirmation = await menus.Confirm(embed).prompt(ctx)
            if confirmation == menus.Confirmation.CONFIRM:
                for item in role:
                    ctx.db.query(Role).filter(Role.rankid==item.rankid).delete()
                    await ctx.success(f"Role Deleted",f"Removed Role {item.rankname} from table")
            elif confirmation == menus.Confirmation.CANCEL:
                await ctx.error("Canceled", "Role Removal Canceled")
        except Exception:
            raise UserError("Prisma Error", f"Failed to remove role {name}")

        
    # Edit properties of a role in the table #
    @rolemgr.command(pass_context=True) 
    async def joinable(self, ctx: Context, name: str):
        role = await self.queryrole(ctx, name)
        rolenames = "\n".join([item.rankname for item in role])
        for item in role:
            item.joinable = not item.joinable
            ctx.db.commit()
        await ctx.success(f"Joinables Updated",
                          f"**Changed:** {rolenames}")

    @rolemgr.command(pass_context=True)
    async def pingable(self, ctx: Context, name: str):
        role = await self.queryrole(ctx, name)
        rolenames = "\n".join([item.rankname for item in role])
        for item in role:
            item.pingable = not item.pingable
            ctx.db.commit()
            
        await ctx.success(f"Pingables Updated",
                          f"**Changed:** {rolenames}")

    @rolemgr.command(pass_context=True)
    async def addtype(self, ctx: Context, name: str, roletype: str):
        '''Fuction to add or change a role's type.
        Usage: p!rolemgr addtype <rolename> <type>'''
        role = await self.queryrole(ctx, name)
        rolenames = "\n".join([item.rankname for item in role])
        if roletype.upper() not in RoleType.__dict__:
            raise UserError("Prisma Error", f"Requested type {roletype} is invalid!")
        for item in role:
            if item.ranktype is not None:
                embed = style.info_embed(
                    "Confirm Change",
                    f"Role {name} already has type {roletype}.  Replace?",
                    title_mode="description")
                confirmation = await menus.Confirm(embed).prompt(ctx)
                if confirmation == menus.Confirmation.CANCEL:
                    await ctx.send(f"Type change operation cancelled")
                if confirmation == menus.Confirmation.CONFIRM:
                    item.ranktype = roletype.upper()
                    ctx.db.commit()
            item.ranktype = roletype.upper()
        await ctx.success(f"Roletype Updated",
                          f"**Changed:** {rolenames} to type {roletype}")

    #@rolemgr.command(pass_context=True)
    #async def type(self, ctx: Context, name: str):
    # Function to apply a type to a role/roles

    @rolemgr.command(pass_context=True)
    async def alias(self, ctx: Context, rolename: str, alias: str):
    # Function to apply an alias to a role/roles
        dbrole = ctx.db.query(Role).filter(Role.rankname==rolename).all()
        print(f"dbrole: {dbrole} ranktype: {dbrole[0].ranktype}")
        if dbrole[0].ranktype != None:
            if dbrole[0].ranktype in [RoleType.COUNTRY, RoleType.PRONOUN, RoleType.GENDER]:
                ranktype = str(dbrole[0].ranktype)[9:]
                tag = ranktype[0]+ranktype[1:].lower()+": "
                print("tag: "+tag)
                if tag not in alias:
                    alias = tag + alias
                print(f"alias: {alias}")
        entry = Alias(rankid=dbrole[0].rankid,
                      rankname=alias)
        try:
            ctx.db.add(entry)
            ctx.db.flush()
        except Exception:
            raise UserError("Aliasing Failed",
                            f"Prisma failed while attempting to enter alias {alias} for {dbrole.rankname} in the database!") 
        
        
    # Lookup information on a role in the table #
    @rolemgr.command(pass_context=True)
    async def lookup(self, ctx: Context, name:str):
        roles = await self.queryrole(ctx, name)
        if roles[0] == None:
            raise UserError(f"Unknown Role",
                            f"Cannot find role {name}")
        for role in roles:
            pingable = role.pingable 
            joinable = role.joinable
            msg = f"id: {role.rankid}\n Opt-in: {joinable}\n Mentionable: {pingable}\n"
            if role.ranktype is not None:
                roletype = role.ranktype
                msg += f"Role Type: {roletype}"
            embed = style.info_embed(
                "Role Information",
                title_mode="description")
            embed.add_field(name=f"{role.rankname}",
                            value=msg)
            await ctx.send(embed=embed)

        
    # Add any roles in the server that don't already exist in the table #
    @rolemgr.group(pass_context=True)
    async def sync(self, ctx: Context, suppress: bool = True):
        if ctx.invoked_subcommand is None:
            server_roles = ctx.guild.roles
            synced_roles = []
            for server_role in server_roles:
                    role = ctx.db.query(Role).get(server_role.id)
                    if role == None:
                        parsedname = "".join(server_role.name.lower().replace(" ",""))
                        if parsedname in ctx.bot.config.roles.user.optin:
                            can_join = True
                            can_ping = ctx.bot.config.roles.user.optin[parsedname]
                        else:
                            can_join = False
                            can_ping = False
                        roletraits = (server_role, can_join, can_ping)
                        synced_roles.append(roletraits)
            embed = style.info_embed(
                "Sync Tables",
                title_mode="description"
                )
            if len(synced_roles) == 0:
                embed.add_field(name="Roles", value="No roles found to be out of sync")
                await ctx.send(embed=embed)
            else:
                rolelist = []
                names = ""
                #msg = "\n".join([role[0].name for role in synced_roles])
                embed.add_field(name="Confirm Role Sync", value=f"Sync Roles with Server?")
                confirmation = await menus.Confirm(embed).prompt(ctx)
                if confirmation == menus.Confirmation.CONFIRM:
                    for role in synced_roles:
                        try:
                            await self.addentry(ctx, role[0], role[1], role[2])
                        except Exception:
                            raise UserError("Prisma Error", f"Failed while attempting to sync role {role[0]}")            
                    if suppress == True:
                        embed = style.success_embed(
                            "Sync Tables",
                            title_mode="description")
                        embed.add_field(name=f"Roles Synced", value="Server and Bot roles synced!")
                        await ctx.send(embed=embed)
                    else:    
                        for rolename in [role[0].name for role in synced_roles]:
                            if (len(names) + len(rolename+'\n') < 1024):
                                names+=(rolename+'\n')
                            else:
                                rolelist.append(names)
                                names=(rolename+'\n')
                        rolelist.append(names)
                        for names in rolelist:
                            #msg = "".join(names)
                            #print(len(names))
                            embed = style.success_embed(
                                "Sync Tables",
                                title_mode="description")
                            embed.add_field(name=f"Roles Synced",value=names)
                            await ctx.send(embed=embed)
                else:
                    embed = style.info_embed(
                        "Sync Tables",
                        title_mode="description")
                    embed.add_field(name="Sync Request", value="Sync Request Canceled")
                    await ctx.send(embed=embed)
    
    # Audit Roles by comparing the Role Table and the Server Roles #
    @rolemgr.command(pass_context=True)
    async def audit(self, ctx: Context, suppress=True):
        server_roles = [role.name for role in ctx.guild.roles]
        database_roles = [role.rankname for role in ctx.db.query(Role).all()]
        allroles = server_roles+list(set(database_roles)-set(server_roles))
        server = []
        database = []
        for role in allroles:
            if role in server_roles:
                server.append(":white_check_mark:")
            else:
                server.append(":x:")
            if role in database_roles:
                database.append(":white_check_mark:")
            else:
                database.append(":x:")

        if suppress == True:
            indexes = []
            for i, (serv, db) in enumerate(zip(server, database)):
                if (serv == ":x:") or (db ==":x:"):
                    indexes.append(i)
            allroles = [allroles[index] for index in indexes]
            server = [server[index] for index in indexes]
            database = [database[index] for index in indexes]
        if len(allroles) == 0:
            embed = style.info_embed(
                "Role Table Audit",
                title_mode="Description")
            embed.add_field(
                name="Audit Results", value="All Roles Synced")
            await ctx.send(embed=embed)
        else:
            carrier = ""
            rolenames = []
            serversymbs = []
            dbsymbs = []
            i = 0
            for role in allroles:
                if i < 25: 
                    carrier+=(role+'\n')
                    i+=1
                else:
                    rolenames.append(carrier)
                    carrier =(role+'\n')
                    i=1
                
            rolenames.append(carrier)
            carrier = ""
            i = 0
            for symbol in server:
                if i < 25:
                    carrier+=(symbol+'\n')
                    i+=1
                else:
                    serversymbs.append(carrier)
                    carrier = symbol+'\n'
                    i=1
            serversymbs.append(carrier)
            carrier = ""
            i = 0
            for symbol in database:
                if i < 25: 
                    carrier+=(symbol+'\n')
                    i+=1
                else:
                    dbsymbs.append(carrier)
                    carrier = (symbol+'\n')
                    i=1
            dbsymbs.append(carrier)

            for (role, servsymb, dbsymb) in zip(rolenames, serversymbs, dbsymbs):
                embed = style.info_embed(
                    "Role Table Audit",
                    title_mode="description")
                embed.add_field(
                    name="__Role Name__", value=role, inline = True)
                embed.add_field(
                    name="__Server__", value=servsymb, inline = True)
                embed.add_field(
                    name="__Bot__", value=dbsymb, inline = True)
                await ctx.send(embed=embed)
            

        
    # Delete all table entries then run the sync command #
    @rolemgr.command(pass_context=True)
    async def revert(self, ctx: Context):
            embed = style.info_embed(
                    "Confirm Reversion",
                    f"**WARNING:** Reversion will delete the role table and resync from the server & config file.\nAre you sure?",
                    title_mode="description")
            confirmation = await menus.Confirm(embed).prompt(ctx)
            if confirmation == menus.Confirmation.CONFIRM:
                ctx.db.query(Alias).delete()
                ctx.db.query(Role).delete()
                await self.sync(ctx)
            elif confirmation == menus.Confirmation.CANCEL:
                await ctx.error("Canceled", "Role Table Reversion Canceled")


    #######################        
    # DB Helper Functions #
    #######################
    
    # Add role to table #
    async def addentry(self, ctx: Context, role: discord.role.Role, can_join: bool = False, can_ping: bool = False):
        '''Helper function to add a Discord.role object to the bot database as a new entry'''
        unlabeledromantic = ["Panromantic", "Heteroromantic", "Homoromantic",
                             "Grey-Aromantic", "Demiromantic", "Biromantic", "Aromantic"]
        unlabeledsexuality = ["Polysexual", "Pansexual", "Lesbian", "Heterosexual",
                          "Heteroflexible", "Homoflexible", "Homosexual",
                          "Gynephile", "Grey-Asexual", "Gay", "Demisexual",
                          "Bisexual", "Asexual", "Androphile", "Abrosexual"]
        roletype = None
        #print("Break 1")
        if "Country" in role.name:
            roletype = "COUNTRY"
        if "Gender" in role.name:
            roletype = "GENDER"
        if "Pronoun" in role.name:
            roletype = "PRONOUN"
        if role.name in ["Over 18", "Over 16", "Under 16"]:
            roletype = "AGE"
        if "Romantic Orientation" in role.name or role.name in unlabeledromantic:
            roletype = "ROMANTIC"
        if "Sexuality" in role.name or role.name in unlabeledsexuality:
            roletype = "SEXUALITY"
        #print("Break 2")
        #print(f"Ranktype={roletype}")
        entry = Role(rankname=role.name,
                     rankid=role.id,
                     joinable=can_join,
                     pingable=can_ping,
                     ranktype=roletype)
        #print("Break 3")
        #print(f"Role to add: {entry.name}, {entry.type}")
        try:
            ctx.db.add(entry)
            ctx.db.flush()
        except Exception:
            raise UserError("Role Entry Failed!",
                            f"Prisma failed while attempting to enter role {role.name} into the database.")

    # Return query object representing role #    
    async def queryrole(self, ctx: Context, name: str):
        try:
            role = ctx.db.query(Role).filter(Role.rankname==name).all()
            if len(role) == 0:
                alias = ctx.db.query(Alias).filter(Alias.rankname==name).all()
                role = ctx.db.query(Role).filter(Role.rankid==alias[0].rankid).all()
            return role
        except Exception:
            raise UserError(f"Role Database Error",
                            f"Role {name} not found")

    # Return Discord.role object representing role #
    #async def getrole(self, ctx: Context, name: str):
    async def getrole(self, ctx: Context, roleid: int):
        #roleid = ctx.db.query(Role).get(id).rankid
        try:
            role = ctx.guild.get_role(roleid)
            return role
        except Exception:
            raise UserError(f"Role Lookup Error",
                            f"Role {name} not found on server")


    ########################
    # User Role Management #
    ########################

    # Role Census #
    @commands.command()
    async def census(self, ctx: Context, logic: str,  *request):
        operators = ("and","or","xor")
        logic = logic.lower().strip()
        if logic not in operators:
            logic = "and"
        roles = []
        carrier = ""
        for item in request: 
            if "," in item or item == request[-1]:
                carrier += item.strip(",")
                roles.append(carrier)
                carrier = ""
            else:
                carrier += item+" "
        for i, role in enumerate(roles):
            roleid = await self.queryrole(ctx, role)
            serverrole = await self.getrole(ctx, roleid[0].rankid)
            roles[i] = serverrole.members
        if logic == "and":
            members = set(roles[0]).intersection(*roles[1:])
        if logic == "or":
            members = list(set().union(*roles))
        if logic == "xor":
            members = set(roles[0]).symmetric_difference(*roles[1:])
        msg = len(members)
        #for member in members:
            #print(member.name)
        embed = style.success_embed(
            "Role Census",
            title_mode="description")
        embed.add_field(name="Results:",value=msg)
        await ctx.send(embed=embed)
        

    # Join Role #
    @commands.command()
    async def join(self, ctx: Context, *request):
            '''Command to join opt-in role.  If valid, adds role to calling user'''
            opt_ins = [r for r in ctx.db.query(Role).filter(Role.joinable == True).all()]
            member = ctx.author
            # Case: No Arg, Return list of joinable roles 
            if len(request) == 0:
                msg = "\n".join([role.rankname for role in opt_ins]) 
                embed = style.info_embed(
                 "Opt-In Roles Available",
                 title_mode="description"
                 )
                embed.add_field(name="Roles:", value=msg)
            
            # Case: User requested a role
            if len(request) !=0:
                requested_roles = []
                requeststring = "".join(request).lower().replace(" ","")
                for role in opt_ins:
                    if re.sub(r"\s+", "",(role.rankname).lower()) == requeststring:
                        requested_roles.append(await self.getrole(ctx, role.rankid))

                    # At least one match found
                if len(requested_roles) != 0:
                    try:
                        await member.add_roles(*requested_roles)
                        msg = "\n".join([r.name for r in requested_roles])
                    except:
                        raise UserError("Prisma Error",
                                        "Failed to add roles")
                elif len(requested_roles) == 0:
                    raise UserError(
                        "Prisma unable to add roles",
                        "No valid Opt-In roles provided")
                embed = style.success_embed(
                    "Roles Added",
                    title_mode="description")
                embed.add_field(name="Roles:",value=msg)
            await ctx.send(embed=embed)

            
    # Leave Role #
    @commands.command()
    async def leave(self, ctx: Context, *request):
            '''Command to leave opt-in role.  If valid, removes role to calling user'''
            opt_ins = [r.rankname for r in ctx.db.query(Role).filter(Role.joinable == True).all()]
            member = ctx.author
            if len(request) == 0:
                raise UserError(
                 "Missing Role",
                 "Need to specify a role to leave!")
            if len(request) !=0:
                requested_roles = []
                requeststring = "".join(request).lower()
                for role in [role for role in member.roles if role.name in opt_ins]:
                    if re.search(re.sub(r"\s+", "", role.name.lower()), requeststring):
                        requested_roles.append(role)
                if len(requested_roles) !=0:
                    try:
                        await member.remove_roles(*requested_roles)
                        msg = "\n".join([r.name for r in requested_roles])
                    except:
                        raise UserError("Prisma Error",
                                        "Failed to remove roles")
                elif len(requested_roles) == 0:
                    raise UserError(
                         "Prisma unable to remove roles",
                         f"Either User {member.name} does not have one or more requested roles or\nrole is not Opt-In")
            embed = style.success_embed(
            "Roles Removed",
            title_mode="description",
            )
            embed.add_field(name="Roles:", value=msg)
            await ctx.send(embed=embed)    

            
    # Mention Role #
    @commands.command()
    @commands.guild_only()
    async def mention(self, ctx: Context, *request):
        '''Command to ping a mentionable role with a message.'''
        member = ctx.author
        args = " ".join(request)
        if len(request) != 0:
            to_ping = await self.matchpingables(ctx, *request)
            if len(to_ping) == 1:
                server_role = to_ping[0]
                msg = re.split(server_role.name, args, flags=re.IGNORECASE)[1]
                if msg == '':
                    raise UserError(
                        "Missing Message",
                        "Please include a message with your mention")
                embed = style.info_embed(
                    "Confirm Mention",
                    f"Mention role **@{server_role.name}** with the message:\n**{msg}**\nAre you sure?",
                    title_mode="description")
                confirmation = await menus.Confirm(embed).prompt(ctx)
                if confirmation == menus.Confirmation.CONFIRM:
                    embed = style.info_embed(
                        f"Attention {server_role.mention}!",
                        f"Message from {member.name}:\n{msg}",
                        title_mode="description")
                    await ctx.send(content=f"{server_role.mention}:mega:", embed = embed)
                elif confirmation == menus.Confirmation.CANCEL:
                    await ctx.error("Canceled", "Mention request canceled")
            if len(to_ping) > 1:
                raise UserError(
                    "Too many roles!",
                    "Only mention one role at a time")
            if len(to_ping) == 0:
                raise UserError(
                    "Cannot Mention Role",
                    "Requested role is not pingable") 
        if len(request) == 0:
            raise UserError(
                "Prisma Error",
                "Role & Message required!")

        
    ############################    
    # Generic Helper Functions #
    ############################

    # Check if Opt-In #
    async def matchoptins(self, ctx: Context, *request):
        '''Check if arbitrary request string matches names of opt-in roles and returns all matches as role object list'''
        opt_ins = [r for r in ctx.db.query(Role).filter(Role.joinable == True).all()]
        requeststring = "".join(request).lower()
        matches = []
        for role in opt_ins:
            if re.search(re.sub(r"\s+", "",role.rankname.lower()), requeststring):
                matches.append(await self.getrole(ctx, role.rankid))
        return matches

    
    # Check if Pingable #
    async def matchpingables(self, ctx: Context, *request):
        '''Check if arbitrary request string matches names of pingable roles and returns all matches as role object list'''
        pingables = [r for r in ctx.db.query(Role).filter(Role.pingable == True).all()]
        requeststring = "".join(request).lower()
        matches = []
        for role in pingables:
             if re.search(re.sub(r"\s+", "",role.rankname.lower()), requeststring):
                 matches.append(await self.getrole(ctx, role.rankid))
        return matches

    
setup = make_cog_setup(RoleCog)

