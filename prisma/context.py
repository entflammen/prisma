from discord.ext import commands
from sqlalchemy.orm import Session

from prisma import Bot


class Context(commands.Context):
    bot: Bot

    def __init__(self, session: Session, **kwargs):
        self.db = session
        super().__init__(**kwargs)
