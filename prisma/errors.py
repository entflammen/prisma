from typing import Optional

import discord
from discord.ext import commands

from prisma import style


class UserError(commands.CommandError):
    """ An exception meant to be displayed to the user who issued a command.
    """

    def __init__(self, title: str, description: Optional[str] = None):
        self.title = title
        self.description = description
        self.message = title + (f"\n{description}" if description is not None else "")

    def to_embed(self) -> discord.Embed:
        return style.error_embed(title=self.title, description=self.description)
