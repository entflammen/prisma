from enum import Enum

import discord
from discord.ext import menus

from prisma import Context, style


class Confirmation(Enum):
    CONFIRM = "confirm"
    CANCEL = "cancel"


class Confirm(menus.Menu):
    def __init__(self, embed: discord.Embed, timeout=60):
        super().__init__()
        self.embed = embed
        self.result = None

    async def send_initial_message(
        self, ctx: Context, _channel: discord.TextChannel
    ) -> discord.Message:
        return await ctx.send(embed=self.embed)

    @menus.button(style.icons.SUCCESS)
    async def on_confirm(self, _payload):
        self.result = Confirmation.CONFIRM
        self.stop()

    @menus.button(style.icons.CANCEL)
    async def on_cancel(self, _payload):
        self.result = Confirmation.CANCEL
        self.stop()

    async def prompt(self, ctx: Context) -> Confirmation:
        await self.start(ctx, wait=True)
        return self.result
