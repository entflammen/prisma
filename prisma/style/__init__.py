import functools
from typing import Literal, Union

import discord

from . import colours, icons
from .colours import Colour
from .icons import Icon

__all__ = ["Colour", "Icon"]

TitleMode = Union[Literal["normal"], Literal["description"]]


def embed_factory(icon, colour):
    def factory(title, description="", title_mode="normal"):
        title_contents = f"{icon} {title}"

        if title_mode == "description":
            actual_title = ""
            actual_description = f"**{title_contents}**"
            if description:
                actual_description += f"\n\n{description}"
        else:
            actual_title = title_contents
            actual_description = description

        return discord.Embed(
            colour=colour, title=actual_title, description=actual_description
        )

    return factory


EMBED_STYLES = {
    "success": (icons.SUCCESS, colours.GREEN),
    "info": (icons.INFO, colours.BLUE),
    "warning": (icons.WARNING, colours.ORANGE),
    "error": (icons.ERROR, colours.RED),
    "unknown": (icons.UNKNOWN, colours.PURPLE),
}

EMBED_FACTORIES = {
    style_name: embed_factory(icon, colour)
    for style_name, (icon, colour) in EMBED_STYLES.items()
}

for name, factory in EMBED_FACTORIES.items():
    globals()[f"{name}_embed"] = factory


def monkeypatch_messageable():
    """ Add convenience methods to :class:`discord.abc.Messageable` for sending
    small, consistently styled embeds.
    """
    for style_name, factory in EMBED_FACTORIES.items():

        async def send_embed(
            self, embed_maker, title, description=None, title_mode="normal"
        ):
            await self.send(embed=embed_maker(title, description, title_mode=title_mode))

        method = functools.partialmethod(send_embed, factory)

        setattr(discord.abc.Messageable, style_name, method)
