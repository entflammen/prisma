from hypothesis import strategies as st

from .. import Colour, Icon, icons

st.register_type_strategy(
    Icon, st.sampled_from([value for _, value in icons.all_constants()])
)

st.register_type_strategy(Colour, st.builds(Colour, value=st.integers(0, 0xFFFFFF)))
