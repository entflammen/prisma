from datetime import timedelta

from hypothesis import given, infer

from ..util import FriendlyTimeDelta, split_timedelta


class TestFriendlyTimedelta:
    @given(td=infer)
    def test_parse_timedelta(self, td: timedelta):
        parsed = FriendlyTimeDelta.parse(td)

        assert parsed == td

    @given(td=infer)
    def test_parse_dict(self, td: timedelta):
        dictionary = split_timedelta(td)

        parsed = FriendlyTimeDelta.parse(dictionary)

        assert parsed == timedelta(**dictionary)


@given(td=infer)
def test_split_timedelta_roundtrips(td: timedelta):
    result = timedelta(**split_timedelta(td))

    assert result == td
